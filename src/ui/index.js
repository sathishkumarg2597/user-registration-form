// NPM IMPORTS
import React from 'react';
import { BrowserRouter as Router,Route, Switch } from 'react-router-dom';

//COMPONENTS IMPORT
import PersonalDetails from "./pages/PersonalDetails/PersonalDetails";
import CompanyDetails from './pages/CompanyDetails/CompanyDetails';
import EmailVerification from './pages/Verification/EmailVerification';
import SuccessPage from './pages/SuccessPage/SuccessPage';

const Content = () =>{

    return <Router>
                <Switch>
                    <Route exact path="/" component={ PersonalDetails }/>
                    <Route exact path="/company_details" component={CompanyDetails}/>
                    <Route exact path="/email_verification" component={EmailVerification}/>
                    <Route exact path="/success" component={SuccessPage}/>
                </Switch>
            </Router>

}

export default Content;