import styled from "styled-components";

export const Container = styled.div `
    width:100%;
    height:100vh;
    display:flex;
    align-items:center;
    background-color:RGBA(46,75,100,0.03);
    flex-direction:column;
    hr{
        background-color: #ECECEC;
        border: 1px solid #ECECEC;
    }
`;

export const Heading = styled.div `
    width:100%;
    display:flex;
    justify-content:center;
    margin: 2rem;
    flex-direction:column;
    align-items:center;
    h2{
        font-family: Lato;
        margin:0 0 1rem 0;
        font-style: normal;
        font-weight: bold;
        font-size: 36px;
        line-height: 43px;
    }

    h3{
        width:442px;
        text-align:center;
        font-family: Lato;
        font-style: normal;
        font-weight: normal;
        font-size: 15px;
        line-height: 24px;
    }
`;

export const FormContainer = styled.form `
    width: 508px;
    display:flex;
    flex-direction:column;
    margin:0 0 30px 0;
    border-radius: 3px;
    justify-content:center;
    background-color:#FFFFFF;
`;

export const FormItem = styled.div `
    width:100%;
    display:flex;
    flex-direction:column;
    justify-content:start;
    margin: 30px 30px 0 30px;
    div{
        display:flex;
        flex-direction:row;
    }
`;

export const Label = styled.h1 `
    font-family: Lato;
    font-style: normal;
    font-weight: normal;
    margin: 0 0 15px 0;
    font-size: 15px;
    line-height: 18px;
    color: rgba(10, 9, 9, 0.6);
`;

export const InputField = styled.input `
    width:60px;
    height:60px;
    margin:0 37px 0 0;
    text-align:center;
    background: #FFFFFF;
    border: 1px solid #CECECE;
    box-sizing: border-box;
    border-radius: 3px;
    font-family: Lato;
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 18px;
    &:focus{
        outline: none;
        border: 1px solid #ED5901;
    }
`;

export const Button = styled.div `
    width:100%;
    display:flex;
    justify-content:start;
    margin: 30px 30px 30px 30px;
`;

export const Back = styled.button `
    width:97px;
    height:40px;
    align-items:center;
    margin: 0 15px 0 0;
    outline:none;
    background: #F4F6F7;
    color: #0A0909;
    font-family: Lato;
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 18px;
    border: 1px solid #F4F6F7;
    box-sizing: border-box;
    border-radius: 3px;
    cursor:pointer;
`;

export const OTP = styled.button `
    width:333px;
    height:40px;
    align-items:center;
    background: #ED5901;
    outline:none;
    font-family: Lato;
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 18px;
    border: 1px solid #F4F6F7;
    box-sizing: border-box;
    border-radius: 3px;
    cursor:pointer;
`;

export const Info = styled(Heading)`
margin:15px 0 15px 0;
h3{
    width:402px;
    text-align:center;
    font-family: Lato;
    font-style: normal;
    font-weight: normal;
    font-size: 15px;
    line-height: 26px;
}
b{
    font-family: Lato;
    font-weight: normal;
    color: #ED5901; 
}
`;