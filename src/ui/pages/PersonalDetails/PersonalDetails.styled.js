import styled from "styled-components";


export const Container = styled.div `
    width:100%;
    display:flex;
    justify-content:center;
    align-items:center;
    background-color:RGBA(46,75,100,0.03);
    flex-direction:column;
`;

export const Heading = styled.div `
    width:100%;
    display:flex;
    justify-content:center;
    margin: 2rem;
    flex-direction:column;
    align-items:center;
    h2{
        font-family: Lato;
        margin:0 0 1rem 0;
        font-style: normal;
        font-weight: bold;
        font-size: 36px;
        line-height: 43px;
    }

    h3{
        font-family: Lato;
        font-style: normal;
        font-weight: normal;
        font-size: 15px;
        line-height: 18px;
    }
`;

export const FormContainer = styled.form `
    width: 508px;
    display:flex;
    flex-direction:column;
    border-radius: 3px;
    justify-content:center;
    background-color:#FFFFFF;
`;

export const FormItem = styled.div `
    width:100%;
    display:flex;
    flex-direction:column;
    justify-content:start;
    margin: 30px 30px 0 30px;

`;

export const FormElement = styled.div `
    position:relative;
    display:flex;
    align-items:center;
    justify-content:start;
    & input[type="radio"]{
        display:none;
    }
    & input[type="radio"]:checked + label{
        border: none;
        color: #ED5901;
        background: rgba(237, 89, 1, 0.1);
    }
    label{
        display:flex;
        justify-content:center;
        align-items:center;
        width:89px;
        height:40px;
        margin:0 15px 0 0;
        background: #FFFFFF;
        border: 1px solid #ECECEC;
        box-sizing: border-box;
        border-radius: 3px;
        font-family: Lato;
        font-style: normal;
        font-weight: 600;
        font-size: 15px;
        line-height: 18px;
    }
    & .floatLabel{
        display:flex;
        justify-content:space-evenly;
        align-items:center;
        width:74px;
        height:40px;
        background-color:rgba(191, 195, 191, 0.1);;
        position:absolute;
        left:0;
        top:0;
        z-index:1;
        font-family: Lato;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 19px;
    }

    & .floatLabelCountry{
        display:flex;
        justify-content:space-evenly;
        align-items:center;
        width:45px;
        height:40px;
        position:absolute;
        left:0;
        top:0;
        z-index:1;
    }

    & .phoneInput{
        padding: 0 0 0 89px;
    }

    
    & .countrySelect{
        padding: 0 0 0 45px;
    }
`;

export const Label = styled.h1 `
    font-family: Lato;
    font-style: normal;
    font-weight: normal;
    margin: 0 0 9px 0;
    font-size: 15px;
    line-height: 18px;
    color: rgba(10, 9, 9, 0.6);
`;

export const InputField = styled.input `
    width:448px;
    height:40px;
    padding: 11px 0 11px 15px;
    background: #FFFFFF;
    border: 1px solid #CECECE;
    box-sizing: border-box;
    border-radius: 3px;
    font-family: Lato;
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 18px;
    &:focus{
        outline: none;
        border: 1px solid #ED5901;
    }
`;

export const SelectItem = styled.select `
    width:448px;
    height:40px;
    padding: 0 0 0 15px;
    background: #FFFFFF;
    border: 1px solid #CECECE;
    box-sizing: border-box;
    border-radius: 3px;
    font-family: Lato;
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 18px;
    & :after{
        color: #0ebeff;
    }
    &:focus{
        outline: none;
        border: 1px solid #ED5901;
    }

`;

export const BigButton = styled.input `
    justify-content:center;
    width:445px;
    height:40px;
    margin:30px 30px 30px 30px;
    align-self:center;
    background: #ED5901;
    border-radius: 3px;
    border:none;
    outline:none;
    cursor: pointer;
    font-family: Lato;
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 18px;
    color: #FFFFFF;
`;

export const Footer = styled.div `
    width:100%;
    display:flex;
    justify-content:center;
    margin: 15px 0 26px 0;
    font-family: Lato;
    font-style: normal;
    font-weight: normal;
    font-size: 15px;
    line-height: 26px;
    color: rgba(10, 9, 9, 0.6);
    h3{
        cursor:pointer;
        font-family: Lato;
        font-style: normal;
        font-weight: normal;
        margin: 0 0 0 3px;
        font-size: 15px;
        line-height: 26px;
        color: #ED5901;
    }
`;

export const Error = styled.h1 `
    font-family: Lato;
    font-style: normal;
    font-weight: normal;
    font-size: 9px;
    line-height: 18px;
    color: red;
`;

export const FlagIcon = styled.img `
    width:18px;
    height:18px;
    border-radius:50%;
`;