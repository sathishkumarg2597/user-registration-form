// NPM IMPORTS
import React from 'react';


//COMPONENTS IMPORT


//MEDIA 
import Success from "../../../media/success.png"

//STYLES
import { Container, Image, Content } from "./SuccessPage.styled";

const SuccessPage = () =>{

    return <>
    <Container>
        <div>
        <Image>
            <img src={Success} />
        </Image>
        <Content>
            <h1>Thank You!</h1>
            <p>Your account has been successfully registered!</p>
        </Content>
        </div>
    </Container>
    </>

}

export default SuccessPage;