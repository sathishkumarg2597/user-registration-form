import styled from "styled-components";


export const Container = styled.div `
    width:100%;
    height:100vh;
    display:flex;
    justify-content:center;
    align-items:center;
    background-color:RGBA(46,75,100,0.03);
    div{
        width:60%;
        display:flex;
        justify-content:center;
    }
`;

export const Image = styled.div `
    width:50%;
    display:flex;
    justify-content:center;
    align-items:center;
    img{
        max-width:25rem;
    }
`;

export const Content = styled.div `
    width:50%;
    display:flex;
    justify-content:center;
    align-items:center;
    flex-direction:column;
    h1{
        font-family: Lato;
        margin:0 0 1rem 0;
        font-style: normal;
        font-weight: bold;
        font-size: 50px;
        line-height: 43px;
    }
    p{
        font-family: Lato;
        margin:0 0 1rem 0;
        font-style: normal;
        font-weight: normal;
        font-size: 20px;
        color:red;
        line-height: 43px;
    }
`;