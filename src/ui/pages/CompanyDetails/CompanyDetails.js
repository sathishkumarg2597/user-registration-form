// NPM IMPORTS
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from "react-hook-form";

//COMPONENTS IMPORT
import NavBar from "../NavBar/Navbar";

//MEDIA
import ImagePlcaeHolder from "../../../media/placeholder.svg"

//STYLES
import {Container, Heading, FormContainer, Image, LogoLabel, FormItem, Label, InputField, Terms, CheckBox, Text, Button, Back, OTP, Error } from "./CompanyDetails.styled";

var bool = false;

const CompanyDetails = () =>{
    const history= useHistory();

    const [year,setYear] = useState('');
    
    const goBack = () =>{
        history.push("/");
    }

    const { register, handleSubmit, errors } = useForm();

    const handleChange = (event) => {
        const re = /^[0-9\b]+$/;
          if (event.target.value === '' || re.test(event.target.value)) {
                setYear(event.target.value);
          }
      }

    const onsubmit = (data) =>{
        if(data){
            history.push("/email_verification");
            localStorage.setItem("isValidCompanyDetails",JSON.stringify(!bool));
            localStorage.setItem("CompanyDetails",JSON.stringify(data));
        }
    }

    useEffect(()=>{
        localStorage.setItem("isValidCompanyDetails",JSON.stringify(bool));
        if(!JSON.parse(localStorage.getItem("isValidPersonalDetails")))
        history.push("/");
    });
    return <>
    <NavBar active="CompanyDetails" completed="PersonalDetails"/>
    <Container>
        <Heading>
            <h2>Add your company details</h2>
            <h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3> 
        </Heading>
        <FormContainer method="post" onSubmit={handleSubmit(onsubmit)}>
            <Image>
                <img src={ImagePlcaeHolder}/>
                <LogoLabel>Upload your company logo</LogoLabel>
            </Image>
            <FormItem>
                <Label>Company Name</Label>
                <InputField type="text" name="company_name" ref={register({required:"This field is required"})} />
                {errors.company_name && <Error>{errors.company_name.message}</Error>}
            </FormItem>
            <FormItem>
                <Label>Email id</Label>
                <InputField type="text" name="email_id" ref={register({
                        required: "This field is required",
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: "Invalid email address"
                                }   })} />
                {errors.email_id && <Error>{errors.email_id.message}</Error>}
            </FormItem>
            <FormItem>
                <Label>Job Title</Label>
                <InputField  type="text" name="job_title" ref={register({required:"This field is required"})} />
                {errors.job_title && <Error>{errors.job_title.message}</Error>}
            </FormItem>
            <FormItem>
                <Label>Years of Experience</Label>
                <InputField value={year} onChange={handleChange} maxLength="2"   type="text" name="years" ref={register({required:true, maxLength: 3})} />
                {errors.years && errors.years.type === "required" && <Error>This field is required</Error>}
            </FormItem>
            <Terms>
                <CheckBox type="checkbox" name="accept_terms" ref={register({required:true})}></CheckBox>
                <Text>
                I accept the
                <b>Terms and Conditions</b>
                </Text>
            </Terms>
            <Button>
                <Back onClick={goBack}>Back</Back>
                <OTP>Send OTP</OTP>
            </Button>
        </FormContainer>
    </Container>
    </>

}

export default CompanyDetails;