import styled from "styled-components";


export const Container = styled.div `
    width:100%;
    display:flex;
    justify-content:center;
    align-items:center;
    background-color:RGBA(46,75,100,0.03);
    flex-direction:column;
`;

export const Heading = styled.div `
    width:100%;
    display:flex;
    justify-content:center;
    margin: 2rem;
    flex-direction:column;
    align-items:center;
    h2{
        font-family: Lato;
        margin:0 0 1rem 0;
        font-style: normal;
        font-weight: bold;
        font-size: 36px;
        line-height: 43px;
    }

    h3{
        font-family: Lato;
        font-style: normal;
        font-weight: normal;
        font-size: 15px;
        line-height: 18px;
    }
`;

export const FormContainer = styled.form `
    width: 508px;
    display:flex;
    flex-direction:column;
    margin:0 0 30px 0;
    border-radius: 3px;
    justify-content:center;
    background-color:#FFFFFF;
`;

export const FormItem = styled.div `
    width:100%;
    display:flex;
    flex-direction:column;
    justify-content:start;
    margin: 30px 30px 0 30px;
`;

export const Image = styled.div `
    width:100%;
    display:flex;
    justify-content:start;
    align-items:center;
    margin: 30px 15px 0 30px;
    img{
        max-width:80px;
        max-height:80px;
        background: #F8F9FA;
        border: 1px dashed rgba(46, 75, 100, 0.1);
        border-radius:50%;
        margin: 0 15px 0 0;
        background-repeat: no-repeat;
        background-size: cover;
    }
`;

export const Label = styled.h1 `
    font-family: Lato;
    font-style: normal;
    font-weight: normal;
    margin: 0 0 9px 0;
    font-size: 15px;
    line-height: 18px;
    color: rgba(10, 9, 9, 0.6);
`;

export const LogoLabel = styled(Label) `
    color: #ED5901;
`;

export const InputField = styled.input `
    width:448px;
    height:40px;
    padding: 11px 0 11px 15px;
    background: #FFFFFF;
    border: 1px solid #CECECE;
    box-sizing: border-box;
    border-radius: 3px;
    font-family: Lato;
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 18px;
    &:focus{
        outline: none;
        border: 1px solid #ED5901;
    }
`;

export const Terms = styled(FormItem)`
    flex-direction: row;
    align-items:center;
`    
export const Text = styled(Label)`
    margin:0;
    display:flex;
    align-items:center;
    b{
        font-family: Lato;
        font-style: normal;
        font-weight: normal;
        font-size: 15px;
        line-height: 26px;
        margin: 0 0 0 2px;
        color: #ED5901;
    }
`

export const CheckBox = styled.input `
    width:24px;
    height:24px;
    background: rgba(206, 206, 206, 0.098066);
    border: 1px solid #CECECE;
    box-sizing: border-box;
    border-radius: 3px;
    margin: 0 16px 0 0;
`;

export const Button = styled.div `
    width:100%;
    display:flex;
    justify-content:start;
    margin: 30px 30px 30px 30px;
`;

export const Back = styled.button `
    width:97px;
    height:40px;
    align-items:center;
    margin: 0 15px 0 0;
    background: #F4F6F7;
    color: #0A0909;
    outline:none;
    font-family: Lato;
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 18px;
    border: 1px solid #F4F6F7;
    box-sizing: border-box;
    border-radius: 3px;
    cursor: pointer;
`;

export const OTP = styled.button `
    width:333px;
    height:40px;
    align-items:center;
    background: #ED5901;
    font-family: Lato;
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 18px;
    outline:none;
    border: 1px solid #F4F6F7;
    box-sizing: border-box;
    border-radius: 3px;
    cursor: pointer;
`;

export const Error = styled.h1 `
    font-family: Lato;
    font-style: normal;
    font-weight: normal;
    font-size: 9px;
    line-height: 18px;
    color: red;
`;