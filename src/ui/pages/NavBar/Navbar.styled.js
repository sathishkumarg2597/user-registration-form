import styled from "styled-components";


export const Container = styled.div `
    width:100%;
    height:60px;
    background-color: #2E4B64;
    display:flex;
    justify-content:center;
    align-items:center;
`;

export const NavLinks = styled.div `
    width:50%;
    display:flex;
    justify-content:center;
`;

export const NavLink = styled.div `
    width:60%;
    display:flex;
    justify-content:center;
    align-items:center;
    & .active {
        color: #FFFFFF;
        background-color: #ED5901;
    }

    & .activeTitle {
        font-weight: 300;
        color: #FFFFFF;
    }
`;

export const Title = styled.h3 `
    font-family: 'Lato', sans-serif;
    font-size: 15px;
    font-weight:normal;
    line-height: 18px;
    color: rgba(255, 255, 255, 0.7);
    margin:1rem;
`;

export const Numbering = styled.div `
    width:30px;
    height:30px;
    display:flex;
    align-items:center;
    justify-content:center;
    background-color:#28435A;
    font-family: Lato;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 19px;
    color: rgba(255, 255, 255, 0.7);
    border-radius:50%;
`;